# Pyton 3.7

import pymysql
import pymysql.cursors
import subprocess

# Configuration settings for database connection
host='localhost'
user='pi'
password='wonderwondels'
database='withdb'

# Connecting to database on pi
connection=pymysql.connect(host,user,password,database)
#cursor=connection.cursor()

# Class constructor for creating the object in the mqttwith.py file
class Query(object):
	# Check if uidkey is already set to absent
	def absencecheck(self,uidkey):
		print('__absencecheck__')
		self.uidkey=uidkey
		print(self.uidkey)
		cursor=connection.cursor()
		sql="SELECT checkedIn FROM employee WHERE uid='%s'" % self.uidkey
		cursor.execute(sql)
		print('Checked UID')
		result= cursor.fetchone()
		#cursor.close()
		#connection.close()
		#print('result: %s' % result)
		#print (type(result))
		result=str(result)
		#print (type(result))
		print('result: %s' % result)
		# Return 0 if person is absent
		absent='(0,)'
		present='(1,)'
		blocked='(2,)'
		error='None'
		if result == absent:
			print('Setting to present')
			sql="UPDATE employee SET checkedIn=1 WHERE uid='%s'" % self.uidkey
			cursor.execute(sql)
			connection.commit()
			print('Set to present, success')
			#setpresent(self.uidkey)
			return 1
		# Return 1 if person is present
		elif result == present:
			print('Error: already logged in')
			print('Setting to absent')
			sql="UPDATE employee SET checkedIn=0 WHERE uid='%s'" % self.uidkey
			cursor.execute(sql)
			connection.commit()
			print('Set to absent, success')
			#setabsent(uidkey)
			return 2
		# Return 1 if uidkey is not found in database
		elif result == error:
			# Access denied for blocked card
			print('Card Blocked')
			return 0
		else:
			# Access denied for unknown card
			print('Person Not Found')
			return 3
		cursor.close()
		connection.close()
