#!/bin/bash

#Variables
iterator=0
path='/home/pi/with-server/beeld/aanwezigheid2.0/'

#Go to path
cd $path
#Compile latex
latexdb beamer.tex
#Convert .dvi file to pdf
sudo dvipdf beamer.dvi
#Start background process in zathura beamer.pdf
exec zathura ${path}beamer.pdf --mode=presentation &
#Wait to open up.
sleep 5
xdotool key Home

while true
do
	#Sleep for 3 seconds
	sleep 5

	#Simulate keystroke 'Pg Dn'
	xdotool key Next
	
	#Add 1 to iterator
	iterator=$((iterator+1))

	#Arrives to last slide
	if [ $iterator -eq 4 ]
	then
		#Wait for the last slide
		sleep 5
		#Go to Home page
		xdotool key Home
		#Compile
		sudo latexdb beamer.tex
		#Make pdf
		sudo dvipdf beamer.dvi
		#Go out if
		iterator=0
	fi
done
