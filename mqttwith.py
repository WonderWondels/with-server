# RPi
import time
#from mqttquery import Query
from query import Query
import paho.mqtt.client as mqtt
import subprocess

# Create query script as object
querymaker = Query()
# Configuration:
MQTT_SERVER = 'localhost'
MQTT_PORT = 1883
# Setup MQTT Publication and subscription
MQTT_Publish='/pi/access'
MQTT_Subscribe='/esp/uid'
# MQTT Credentials
MQTT_Username='pi'
MQTT_Password='wonderwondels'
# Setup callback functions that are called when MQTT events happen like
# connecting to the server or receiving data from a subscribed feed.
def on_connect(client, userdata, flags, rc):
   print("Connected with result code " + str(rc))
   # Subscribing in on_connect() means that if we lose the connection and
   # reconnect then subscriptions will be renewed.
   client.subscribe(MQTT_Subscribe)
# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
   print(msg.topic+" "+str( msg.payload))
   # Check if this is a message for the Pi LED.
   if msg.topic == MQTT_Subscribe:
       # Sub-string part of the whole string to distinguish the action code and uidkey
       uidkey=msg.payload
       uidkey=str(uidkey,'utf-8')
       print(uidkey)
       # Look at the message data and perform the appropriate action.
       checker=querymaker.absencecheck(uidkey)
       # If presence is false so absent
       if checker==0:
            # Access denied for blocked card
            # Send command to blink redlight on esp8266
           client.publish(MQTT_Publish, payload='0')
         # if card is not found
       elif checker==1:
           # To check-in the person of the uid
           # print('Set to present Error 101')
           # Send command to esp8266 to blink greenlight
           client.publish(MQTT_Publish, payload='1')
       elif checker==2:
           # If presence is true
           # Check-out the person of the uid
           # print('Set to absent Error 101')
           # Send command to blink greenlight six on esp8266
           client.publish(MQTT_Publish, payload='2')
       elif checker==3:
           # Access denied for unknown card
           client.publish(MQTT_Publish, payload='3')

# Create MQTT client and connect to localhost, i.e. the Raspberry Pi running
# this script and the MQTT server.
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.username_pw_set(MQTT_Username, MQTT_Password)
# Connect to the MQTT server and process messages in a background thread.
client.connect(MQTT_SERVER, MQTT_PORT, 60)
# Main loop to listen for button presses.
client.loop_start()
while True:
    time.sleep(0.02)
print('Script is running, press Ctrl-C to quit...')
